# Galagaga

Le projet galagaga à été dévellopé dans le cadre d'un cours de l'Université de Bretagne Occidental.
Il est déployer grâce au pipeline de gitlab : https://teten_.gitlab.io/projectgalagaga/

## Contrôles

Un fois en jeu il faut utiliser les touches du clavier : 

- Flèche droite : se déplacer à droite
- Flèche gauche se déplacer à gauche
- Espace : tirer

## Easter egg

Un Cheat Code à été implémenté. Il rend le vaisseau invincible pendant 5 secondes. Le code est à entrer comme tel durant une parie de jeu :
```
Haut Haut Bas Bas Gauche Droite Gauche Droite B A Enter
```

const cadence = 10; // temps entre deux frame (en ms)
const width = 600;
const height = 800;
const intervalles = []; // déplacement de tout les elements
let level = 1;

// vaisseau
let nbLife = 3;
let pause = false; // mettre en pause le jeu après une mort
const shipSize = 50;
const vitesse = 8;
const lasers = [];
const ship  = document.createElement('img');

// enemies
const enemySize = 50;
const nbPapillon = 18;
const nbAbeille = 18;
const papillons = [];
var input = "";
const abeilles = [];
const vitesseEnemy = 1;
const tires = [];
let valeurEnemy = 50;

// score
let currentScore = 0;
let lowestScore;

// Haut Haut Bas Bas Gauche Droite Gauche Droite B A Start
const code = "ArrowUpArrowUpArrowDownArrowDownArrowLeftArrowRightArrowLeftArrowRightbaEnter";

let sheet = false;

function setUp(lastScore) {
    lowestScore = lastScore; 
    draw();
    setTimeout(()=> {
        document.getElementById("start").style.visibility = "hidden";
        tireEnemy();
        shot();
        moveEnemy();
        move();
        colison();
        setInterval(() => {
            if (valeurEnemy > 0) {
                valeurEnemy --;
            }
        }, 5000);
    }, 3000);
}

function draw() {
    // création des vies
    const lifeContainer = document.getElementById("life");
    for (let i=1; i<nbLife; i++) {
        life = document.createElement('img');
        life.src = '../img/vaisseau.png';
        life.style =  "width:" + shipSize + "px;height:" + shipSize + "px; margin:5px";
        lifeContainer.appendChild(life);
    }
    // attribution du bon niveau
    document.getElementById("levelStart").textContent = level;
    document.getElementById("levelEnd").textContent = level;
    const levelContainer = document.getElementById("levels");
    let i = 1;
    for (; i+5<=level; i=i+5) {
        levelComplete = document.createElement('img');
        levelComplete.src = "../img/level5.png";
        levelComplete.style = "width:25px; margin:5px";
        levelContainer.appendChild(levelComplete);
    }
    for (; i<level; i++) {
        levelComplete = document.createElement('img');
        levelComplete.src = "../img/level.png";
        levelComplete.style = "width:20px;   margin:5px";
        levelContainer.appendChild(levelComplete);
    }

    createEnemy();
    createShip();
}

// ---------------------SHIP--------------------- //

function createShip() {
    // creation du vaisseau
    const shipContainer = document.getElementById("ship");
    ship.src = '../img/vaisseau.png';
    ship.style = "width:" + shipSize + "px;height:" + shipSize + "px; position: absolute;"
    + "top:740px;"
    + "left:" + ((width/2) - (shipSize/2)) + "px;";
    shipContainer.appendChild(ship);
}

function move() {
    let mvt = 0; // 1: droite, -1: gauche, 0:aucun
    // handle key press
    document.addEventListener('keydown', (event) => {
        input += event.key;
        if (event.key == "ArrowRight") {
            mvt = 1;
        } else if (event.key == "ArrowLeft") {
            mvt = -1;
        }
        for (var i = 0; i < code.length; i++) {
            if (input[i] != code[i] && input[i] != undefined) {
                input = "";
            }
        }
        if (input == code) {
            sheetCode();
            console.log("Easter Egg provided by GGestin.");
            input = "";
        }
    });
    document.addEventListener('keyup', (event) => {
        if (event.key == "ArrowRight" || event.key == "ArrowLeft") {
            mvt = 0;
        }
    });

    setInterval(() => {
        if (!pause) {
            if (mvt === 1) {
                if (getLeftValue(ship) + vitesse <= width-shipSize) {
                    ship.style.left = (getLeftValue(ship) + vitesse)+"px";
                }
            } else if (mvt === -1) {
                if (getLeftValue(ship) - vitesse >= 0) {
                    ship.style.left = (getLeftValue(ship) - vitesse) +"px";
                }
            }
        }
    }, 30);
}

function shot() {
    document.addEventListener('keyup', (event) => {
        if (event.code == "Space" && !pause) {
            laser();
        }
    });
}

function laser() {
    // création du laser
    const gameArea = document.getElementById("gameArea");
	let laser = document.createElement('img');
    laser.src = "../img/laser.png";
	laser.style = "width:12px;height:30px;position:absolute;background-color:yellow;"
    laser.style.left = (getLeftValue(ship) + shipSize/2 ) + "px";
    laser.style.top = (getTopValue(ship) - shipSize/2 ) + "px";
    lasers.push(laser);
    gameArea.appendChild(laser);

    // déplacement du laser
    const interval = setInterval(() => {
        if (!pause) {
            laser.style.top = (getTopValue(laser) - 5) + "px";
        }
    }, cadence);
    intervalles.push(interval);
}

// ---------------------ENEMY--------------------- //

function createEnemy() {
    const enemy = document.getElementById("enemy");

    // Placement des papillon
    for (let i=0; i<nbPapillon; i++) {
        var papillon = document.createElement('img');
        papillon.src = '../img/papillon.png';
        papillon.style = "width:" + enemySize + "px;height:" + enemySize +"px; position: absolute;"
        + "left: " + (i<(nbPapillon/2) ? (i + (i*60)) : ((i-(nbPapillon/2)) + ((i-(nbPapillon/2))*60))) + "px; "
        + "top: " + (i<(nbPapillon/2) ? 5 : 70) + "px;";
        papillons.push(papillon);
        enemy.appendChild(papillon);
    }
    // Placement des abeilles
    for (let i=0; i<nbAbeille; i++) {
        var abeille = document.createElement('img');
        abeille.src = '../img/abeille.png';
        abeille.style = "width:" + enemySize + "px;height:" + enemySize +"px; position: absolute;"
        + "left: " + (i<(nbAbeille/2) ? (i + (i*60)) : ((i-(nbAbeille/2)) + ((i-(nbAbeille/2))*60))) + "px; "
        + "top: " + (i<(nbAbeille/2) ? 140 : 210) + "px;";
        abeilles.push(abeille);
        enemy.appendChild(abeille);
    }
}

function moveEnemy() {
    const rest = width - ((60 * nbPapillon)/2);
    let position = -rest;
    let gauche = false;

    const interval = setInterval(() => {
        if (!pause) {
            if (position > -rest && !gauche) {
                moveLeft();
                position--;
            } else if (position < 0) {
                moveRight();
                position++;
                gauche = true;
            }else {
                moveLeft();
                position--;
                gauche = false;
            }
        }
    }, cadence);
    intervalles.push(interval);
}

function moveLeft() {
    papillons.forEach(papillon => {
        papillon.style.left = (getLeftValue(papillon) - vitesseEnemy) + "px";
    });
    abeilles.forEach(abeille => {
        abeille.style.left = (getLeftValue(abeille) - vitesseEnemy) + "px";
    });
}

function moveRight() {
    papillons.forEach(papillon => {
        papillon.style.left = (getLeftValue(papillon) + vitesseEnemy) + "px";
    });
    abeilles.forEach(abeille => {
        abeille.style.left = (getLeftValue(abeille) + vitesseEnemy) + "px";
    });
}

function tireEnemy() {
    const interval1 = setInterval(() => {
        if (!pause) {
            for (let i=0; i<3; i++) {
                const papillon = papillons[Math.floor(Math.random() * papillons.length)];
                // création du tire
                const gameArea = document.getElementById("gameArea");
                let tire = document.createElement('img');
                tire.src = "../img/laser_enemy.png";
                tire.style = "width:12px;height:30px;position:absolute;"
                tire.style.left = (getLeftValue(papillon) + enemySize/2) + "px";
                tire.style.top = (getTopValue(papillon) + enemySize) + "px";
                tires.push(tire);
                gameArea.appendChild(tire);
        
                // déplacement du tire
                setInterval(() => {
                    if (!pause) {
                        tire.style.top = (getTopValue(tire) + 4) + "px";
                    }
                }, cadence);
            }
        }
    }, 2000);
    intervalles.push(interval1);
    const interval2 = setInterval(() => {
        if (!pause) {
            for (let i=0; i<3; i++) {
                const abeille = abeilles[Math.floor(Math.random() * abeilles.length)];
                // création du tire
                const gameArea = document.getElementById("gameArea");
                let tire = document.createElement('img');
                tire.src = "../img/laser_enemy.png";
                tire.style = "width:6px;height:20px;position:absolute;"
                tire.style.left = (getLeftValue(abeille) + enemySize/2) + "px";
                tire.style.top = (getTopValue(abeille) + enemySize) + "px";
                tires.push(tire);
                gameArea.appendChild(tire);
        
                // déplacement du tire
                setInterval(() => {
                    if (!pause) {
                        tire.style.top = (getTopValue(tire) + 4) + "px";
                    }
                }, cadence);
            }
        }
    }, 1500);
    intervalles.push(interval2);
}

// ---------------------COLISION--------------------- //

function colison() {
    const interval = setInterval(() => {
        if (!pause) {
            colisionEnemy();
            // si la partie est gagné
            if (papillons.length == 0 && abeilles.length == 0) {
                victory();
            }
            colisionShip();
        }
    }, cadence);
    intervalles.push(interval);
}

function colisionEnemy() {
    // création d'un tableau pour tout les ennemies
    const enemies = abeilles.concat(papillons);
    // récupération des scores
    const score = document.getElementById("currentScore");
    const highest = document.getElementById("highestScore");

    lasers.forEach(laser => {
        if (getTopValue(laser) == 0) {
            lasers.splice(lasers.findIndex((elem) => elem === laser), 1);
            laser.remove();
        } else {
            enemies.forEach(enemy => {
                // si il y a colision
                if (getTopValue(laser) < getTopValue(enemy)+enemySize && getTopValue(laser) > getTopValue(enemy) 
                && getLeftValue(laser) <= getLeftValue(enemy)+enemySize && getLeftValue(laser) >= getLeftValue(enemy)) {
                    // suppression des éléments dans chaque tableau
                    lasers.splice(lasers.indexOf(laser), 1);
                    enemies.splice(enemies.indexOf(enemy), 1);
                    if (papillons.indexOf(enemy) != -1) {
                        papillons.splice(papillons.indexOf(enemy), 1);
                        currentScore += valeurEnemy*2;
                    }
                    if (abeilles.indexOf(enemy) != -1) {
                        abeilles.splice(abeilles.indexOf(enemy), 1);
                        currentScore += valeurEnemy;
                    }
                    // suppression des elements
                    laser.remove();
                    enemy.remove();
                    // mise a jour du score
                    score.textContent = currentScore;
                    if (currentScore > highest.textContent) {
                        highest.textContent = currentScore;
                    }
                }
            });
        }
    });
}

function colisionShip() {
    tires.forEach(tire => {
        if (getTopValue(tire)+20 >= height) {
            tire.remove();
            tires.splice(tires.indexOf(tire), 1);
        }
        if ((getTopValue(tire)+20 >= getTopValue(ship) && getTopValue(tire)+20 < getTopValue(ship)+shipSize 
        && getLeftValue(tire) >= getLeftValue(ship) && getLeftValue(tire) < getLeftValue(ship)+shipSize)
        && !sheet) {
            nbLife--;
            tire.remove();
            tires.splice(tires.indexOf(tire), 1);
            ship.remove();
            if (nbLife == 0) {
                gameOver();
            } else {
                document.getElementById('life').children[0].remove();
                pause = true;

                // recentrer et faire clignoter le vaisseau avant de reprendre la partie
                createShip();
                let inter = setInterval(() => {
                    if (pause) {
                        if (ship.style.visibility === "hidden") ship.style.visibility = "visible";
                        else ship.style.visibility = "hidden";
                    } else {
                        ship.style.visibility = "visible";
                        clearInterval(inter);
                    }
                }, 250);

                setTimeout(() => pause = false, 2000);
            }
        }
    });
}

// ---------------------FIN DE PARTIE--------------------- //

function gameOver() {
    clear();
    //affichage du message de fin de partie
    const message = document.getElementById("over");
    message.style.visibility = "visible";

    // saisie du nom si bon score
    if (currentScore > lowestScore) {
        document.getElementById("saisieScore").style.visibility = "visible";
        const saisie = document.getElementById("saisie");
        saisie.value = "";
        document.getElementById("saveScore").onclick = () => {
            let name = saisie.value;
            if (saisie.value === "") {
                name = "anonyme";
            }
            window.location.replace(`../pages/score.html?score=${currentScore}&winner=${name}`);
        };
    } else {
        const score = document.getElementById("tabScore");
        score.style.visibility = "visible";
    }
}

function victory() {
    clear();
    //affichage du message de fin de partie
    const message = document.getElementById("clear");
    message.style.visibility = "visible";
    // lancement du prochain niveau
    setTimeout(() => {
        message.style.visibility = "hidden";
        document.getElementById("life").innerHTML = '';
        document.getElementById("levels").innerHTML = '';
        level ++;
        document.getElementById("start").style.visibility = "visible";
        setUp(lowestScore);
    }, 5000);
}

function clear() {
    // arret de tout les déplacements
    intervalles.forEach(inter => {
        clearInterval(inter);
    });
    // suppression des tires
    tires.forEach(tire => {
        tire.remove();
    });
    lasers.forEach(laser => {
        laser.remove();
    });
}

// Utilitaires
function getLeftValue(div) {
    return parseInt(div.style.left.substring(0, div.style.left.length-2), 10);
}

function getTopValue(div) {
    return parseInt(div.style.top.substring(0, div.style.top.length-2), 10);
}

function sheetCode() {
    sheet = true;
    ship.src = "../img/sheet_ship.png";
    setTimeout(() => {
        sheet = false;
        ship.src = "../img/vaisseau.png";
    }, 5000);
}

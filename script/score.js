function getScore() {
    var queryString = decodeURIComponent(window.location.search);
    if (queryString == "") {
        return null;
    }
    queryString = queryString.substring(1);
    var queries = queryString.split("&");
    let tab = [];
    tab.push(queries[1].split("=")[1]);
    tab.push(queries[0].split("=")[1]);
    return tab;
}